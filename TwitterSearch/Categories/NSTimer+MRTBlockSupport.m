//
//  NSTimer+MRTBlockSupport.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "NSTimer+MRTBlockSupport.h"

@implementation NSTimer (MRTBlockSupport)

+ (NSTimer *)sheduleTimerWithInterval:(NSTimeInterval)interval block:(void(^)())block repeats:(BOOL)repeats
{
    return [self scheduledTimerWithTimeInterval:interval
                                         target:self
                                       selector:@selector(blockInvoke:)
                                       userInfo:[block copy]
                                        repeats:repeats];
}

+ (void)blockInvoke:(NSTimer *)timer
{
    void (^block)() = timer.userInfo;
    if (block) {
        block();
    }
}

@end
