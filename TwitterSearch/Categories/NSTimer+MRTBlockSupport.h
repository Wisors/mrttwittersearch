//
//  NSTimer+MRTBlockSupport.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (MRTBlockSupport)
+ (NSTimer *)sheduleTimerWithInterval:(NSTimeInterval)interval block:(void(^)())block repeats:(BOOL)repeats;
@end
