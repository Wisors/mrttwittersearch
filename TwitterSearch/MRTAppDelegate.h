//
//  MRTAppDelegate.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
