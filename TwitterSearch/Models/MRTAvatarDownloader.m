//
//  MRTAvatarDownloader.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 02/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "MRTAvatarDownloader.h"

@interface MRTAvatarDownloader()
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, strong) NSCache *cache;
@end

@implementation MRTAvatarDownloader

#pragma mark - Singleton Init

+ (MRTAvatarDownloader *)sharedInstanse
{
    static dispatch_once_t onceToken;
    static MRTAvatarDownloader *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initUniqueInstance];
    });
    return sharedInstance;
}

- (MRTAvatarDownloader *)initUniqueInstance
{
    if ((self = [super init])) {
        _downloadQueue = [[NSOperationQueue alloc] init];
        _downloadQueue.maxConcurrentOperationCount = 4;
        _cache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark - Public interface

- (void)loadAsynchronousAvatarByURL:(NSURL *)url
              WithCompletionHandler:(MRTTweetAvatarDownloadHandler)handler
{
    return [self loadAsynchronousAvatarByURL:url
                       WithCompletionHandler:handler
                                 saveToCache:YES];
}

- (void)loadAsynchronousAvatarByURL:(NSURL *)url
              WithCompletionHandler:(MRTTweetAvatarDownloadHandler)handler
                        saveToCache:(BOOL)save
{
    if (!url) return;
    
    UIImage *cachedImage = [self.cache objectForKey:[url absoluteString]];
    if (cachedImage) {
        handler(cachedImage);
    }
    else {
        [self.downloadQueue addOperationWithBlock:^{
            NSData *imageData = [NSData dataWithContentsOfURL:url];
            UIImage *image = nil;
            if (imageData) {
                image = [UIImage imageWithData:imageData];
            }
            
            if (image) {
                if (save) {
                    [self.cache setObject:image forKey:[url absoluteString]];
                }
                handler(image);
            }
        }];
    }
}

@end
