//
//  MRTTwitterAuthorizator.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Accounts/Accounts.h>

/**
 *  Состояния авторизации твиттер аккаунта.
 */
typedef NS_ENUM(NSUInteger, MRTTwitterAuthState) {
    /**
     *  Стартовое состояние. Еще не закончена проверка разрешения на доступ к аккаунту(ам) и наличие.
     */
    MRTTwitterAuthStateUndetermined,
    /**
     *  Аккаунт для авторизации успешно получен и доступен в read-only property twitterAccount.
     */
    MRTTwitterAuthStateSuccess,
    /**
     *  Доступ к твиттер аккаунт(у)ам запрещен пользователем.
     */
    MRTTwitterAuthStateErrorAccessDenied,
    /**
     *  Нет ни одного настроенного твиттер аккаунта.
     */
    MRTTwitterAuthStateErrorAccountMissing
};

/**
 *  Singleton-класс для доступа к объекту ACAcount твиттера. В момент инициализации запускает процедуру поиска привязанного к телефону твиттер аккаунта, в этот момент имеет состояние MRTTwitterAuthStateUndetermined. После успешной авторизации перейдет в состояние MRTTwitterAuthStateSuccess, а аккаунт будет доступен в соответсвующем read-only property. Во всех других состояниях (ошибочных и неавторизованном) property будет содержать nil.
 @warning После возврата из бэкграуда приложения автоматически перепроверяется наличие изменений в настройках твиттер аккаунта и статус может измениться.
 @note Поскольку функционал нашего приложение не зависит от аккаунта пользователя, который его использует, допущено опущение: будет выбран первый доступный аккаунт, привязанный в настройках.
 */

@interface MRTTwitterAuthorizator : NSObject

@property (nonatomic, assign, readonly) MRTTwitterAuthState state;
/**
 *  Доступный твиттер аккаунт.
 * 
 *  @warning Инициализирован значением только при статусе MRTTwitterAuthStateSuccess, во всех других состояних nil.
 */
@property (nonatomic, strong, readonly) ACAccount *twitterAccount;

//Защита от дурака для Singleton
+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

+ (MRTTwitterAuthorizator *)sharedInstanse;

@end
