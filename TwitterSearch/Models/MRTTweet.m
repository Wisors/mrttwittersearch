//
//  MRTTweet.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

NSString *const kTwitterDateFormat = @"EEE MMM d HH:mm:ss Z y";

/**
 *  Ключи в JSON API твиттера для твитта.
 */
NSString *const kTweetKeyText = @"text";
NSString *const kTweetKeyPubDate = @"created_at";
NSString *const kTweetKeyUserInfo = @"user";

/**
 *  Ключи в JSON APU твиттера для пользователя.
 */
NSString *const kTweetKeyNickname = @"screen_name";
NSString *const kTweetKeyAvatarURL = @"profile_image_url";

#import "MRTTweet.h"

@implementation MRTTweet

+ (instancetype)tweetByTweetInfo:(NSDictionary *)tweetInfo
{
    if (!tweetInfo) return nil;
    
    NSString *text = [tweetInfo objectForKey:kTweetKeyText];
    NSString *dateSting = [tweetInfo objectForKey:kTweetKeyPubDate];
    NSDictionary *userInfo = [tweetInfo objectForKey:kTweetKeyUserInfo];
    
    if (!userInfo) return nil;
    
    NSString *nickname = [userInfo objectForKey:kTweetKeyNickname];
    NSString *avaURLString = [userInfo objectForKey:kTweetKeyAvatarURL];
    avaURLString = [avaURLString stringByReplacingOccurrencesOfString:@"_normal" withString:@"_bigger"]; //better quality for image
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:kTwitterDateFormat];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    NSDate *pubDate = [dateFormatter dateFromString:dateSting];

    NSURL *avaURL = [NSURL URLWithString:avaURLString];
    
    return [[MRTTweet alloc] initTweetWithText:text author:nickname avatarURL:avaURL publicationDate:pubDate];
}

- (instancetype)initTweetWithText:(NSString *)text author:(NSString *)author avatarURL:(NSURL *)avatarURL publicationDate:(NSDate *)pubDate
{
    if ((self = [super init]) && [text length] != 0 && [author length] != 0) {
        _text = text;
        _author = ([author hasPrefix:@"@"]) ? author : [@"@" stringByAppendingString:author];
        _avatarURL = avatarURL;
        _pubDate = pubDate;
    }
    
    return self;
}

- (NSString *)pubDateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return (self.pubDate) ? [dateFormatter stringFromDate:self.pubDate] : @"N/A";
}

@end
