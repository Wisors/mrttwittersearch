//
//  MRTTweet.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^MRTTweetAuthorAvatarDownloadHandler)(UIImage *avatar);

@interface MRTTweet : NSObject
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, strong) NSURL *avatarURL;
@property (nonatomic, strong) NSDate *pubDate;
/**
 *  Атрибут для маркировки твитов.
 *  @warning Выставляется создателем.
 */
@property (nonatomic, assign) NSUInteger tag;

/**
 *  Factory метод для парсинга JSON данных о твите в формате API Twitter.
 *
 *  @param tweetInfo Словарь данных о твите.
 *
 *  @return Объект твита. nil, если данные в неверном формате или их недостаточно.
 */
+ (instancetype)tweetByTweetInfo:(NSDictionary *)tweetInfo;

/**
 *  Инициализация твита. Для инициализации требуется как минимум указать текс твита и его автора.
 *
 *  @param text      Текс твита.
 *  @param author    Никнейм автора. (автоматически препеднится "@", если этого не сделано)
 *  @param avatarURL Ссылка на картинку аватара.
 *  @param pubDate   Дата публикации твита.
 *
 *  @return Объект твита, либо nil, если не заданны все обязательные параметры (text и author).
 */
- (instancetype)initTweetWithText:(NSString *)text
                           author:(NSString *)author
                        avatarURL:(NSURL *)avatarURL
                  publicationDate:(NSDate *)pubDate;

/**
 *  Дата публикации в строковом виде по формату "HH:MM". (24 часа)
 *
 *  @return Время строкой, либо строка @"N/A", если время не задано.
 */
- (NSString *)pubDateString;

@end
