//
//  MRTAvatarDownloader.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 02/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^MRTTweetAvatarDownloadHandler)(UIImage *avatar);

@interface MRTAvatarDownloader : NSObject

//Защита от дурака для Singleton
+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

+ (MRTAvatarDownloader *)sharedInstanse;

/**
 *  Запускает -loadAsynchronousAvatarByURL:WithCompletionHandler:handler:saveToCache: с установленным в YES флагом сохраения в кэше.
 *
 *  @param url     Ссылка на картинку
 *  @param handler Блок, выполняемый после получения аватара.
 */
- (void)loadAsynchronousAvatarByURL:(NSURL *)url
              WithCompletionHandler:(MRTTweetAvatarDownloadHandler)handler;

/**
 *  Проверяет наличие аватара в кэше и при наличии сразу исполняет блок завершения. В противном случае запускает асинхронную загрузку, после завершения которой сохраняет аватар в кеше, если установлен флаг save, и выполняет блок завершения.
 *
 *  @param url     Ссылка на картинку
 *  @param handler Блок, выполняемый после получения аватара.
 *  @param save    Если флаг установлен, то аватар сохранится в кэше.
 */
- (void)loadAsynchronousAvatarByURL:(NSURL *)url
              WithCompletionHandler:(MRTTweetAvatarDownloadHandler)handler
                        saveToCache:(BOOL)save;

@end
