//
//  MRTSeachTweetsModel.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

#import "MRTTweet.h"

/**
 *  Модель поиска твиттов по ключу(хэштегу).
 *  Модель подключается к Stream API Twitter, используя предоставленный аккаунт пользователя, и производит живую выдачу твиттов по опреденному хэштегу. Для получения новых твиттов необходимо реализовать методы протокола MRTSearchTweetsModelDelegate.
 *  Некоторые стримы могут быть довольно активными, поэтому скорость поиска можно ограничить (твиттов в секунду) при его запуске.
 */

@class MRTSearchTweetsModel;

@protocol MRTSearchTweetsModelDelegate <NSObject>
/**
 *  Сообщение о новой порции твитов по индексам (на момент сообщения).
 *
 *  @param model   Модель поиска.
 *  @param indexes Индексы новых твитов.
 */
- (void)searchTweetsModel:(MRTSearchTweetsModel *)model didRecieveNewTweetsAtIndexes:(NSIndexSet *)indexes;
/**
 *  Произошло серьезное изменение модели. Например, изменился запрос поиска. Делегату требуется заново выгрузить данные.
 *
 *  @param model Модель поиска.
 */
- (void)searchTweetsModelDidChange:(MRTSearchTweetsModel *)model;
/**
 *  При поиске произошла ошибка.
 *
 *  @param model Модель поиска.
 *  @param error Ошибка.
 */
- (void)searchTweetsModel:(MRTSearchTweetsModel *)model didFailWithError:(NSError *)error;
@end

/**
 *  Дефолтная скорость получения новых твитов.
 */
static const NSUInteger MRTDefaultTweetSpeedLimitPerSercond = 3;

@interface MRTSearchTweetsModel : NSObject

@property (nonatomic, strong) ACAccount *account;
@property (nonatomic, weak) id <MRTSearchTweetsModelDelegate> delegate;

@property (nonatomic, strong, readonly) NSArray *tweets;

/**
 *  Инициализация поисковой модели по аккаунту в Twitter.
 *
 *  @param account Системный аккаунт в Twitter.
 *
 *  @return Объект модели.
 */
- (instancetype)initWithTwitterAccount:(ACAccount *)account;

/**
 *  Запускает -(void)searchKeyword:withTweetSpeedLimitPerSecond: с лимитом MRTDefaultTweetSpeedLimitPerSercond;
 *
 *  @param keyword Строка с хэштегами для поиска.
 */
- (void)searchKeyword:(NSString *)keyword;

/**
 *  Запустить поиск с лимитом скорости подключения твиттов в секунду. В строке keyword находятся все хештеги и по ним запускается поиск. В случае, если не задан ни один хэштег будет вызван метод протокола MRTSearchTweetsModelDelegate - (void)searchTweetsModel:didFailWithError: с ошибкой.
 *  @note Ограничение скорости работает по модели bucket, которая часто используется в сетевом оборудовании: http://www.cisco.com/image/gif/paws/24800/153-a.gif
 *
 *  @param keyword Строка с хэштегами для поиска.
 *  @param limit   Лимит на скорость получения твиттов.
 */
- (void)searchKeyword:(NSString *)keyword withTweetSpeedLimitPerSecond:(NSUInteger)limit;

/**
 *  Очистить модель от текущих сохранненых твиттов.
 */
- (void)clearTweets;

/**
 *  Остановить поиск.
 */
- (void)stop;

@end
