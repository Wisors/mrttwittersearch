//
//  MRTTwitterStreamingSearchAPI.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

#import "MRTTweet.h"

/**
 *  Класс для работы с Streaming-API Twitter. Объекты класса не предначначены для повторного использования, если необходимо подключиться к другому потоку, нужно создать новый объект стрима. В качестве настройки доступны только интервал получения обновлений и лимит на количество твитов в одном обновлении.
 */


/**
 *  Блок получения новых твиттов.
 *
 *  @param newTweets Массив с tweetInfo новых твиттов, содержит не более чем updateTweetsLimit твитов.
 *  @param error     Ошибка стрима.
 */
typedef void (^MRTTwitterSearchStreamUpdateHandler)(NSArray *newTweets, NSError *error);

@interface MRTTwitterSearchStream : NSObject
-(instancetype) init __attribute__((unavailable("init not available, call custom initializer")));

/**
 *  Переодичность, с которой нужно вызывать блок MRTTwitterSearchStreamUpdateHandler.
 */
@property (nonatomic, assign) NSTimeInterval updateInterval;
/**
 *  Лимит на количество новых твиттов при вызове блока MRTTwitterSearchStreamUpdateHandler
 */
@property (nonatomic, assign) NSUInteger updateTweetsLimit;

/**
 *  Factory метод инициализации стрима твиттов по хэштегу.
 *
 *  @param keyword Ключевые слова в формате https://dev.twitter.com/docs/streaming-apis/parameters#track
 *  @param account Аккаунт Twitter
 *  @param handler Блок, вызываемый при получении новых твитов или возникновения ошибки стрима.
 *
 *  @return Объект стрима, либо nil, если не задан один из параметров.
 */
+ (instancetype)streamForSearchKeyword:(NSString *)keyword
                           withAccount:(ACAccount *)account
                      andUpdateHandler:(MRTTwitterSearchStreamUpdateHandler)handler;

- (instancetype)initStreamForSearchKeyword:(NSString *)keyword
                               withAccount:(ACAccount *)account
                          andUpdateHandler:(MRTTwitterSearchStreamUpdateHandler)handler;
/**
 *  Запуск стрима.
 */
- (void)start;
/**
 *  Отсановка стрима.
 */
- (void)close;

@end
