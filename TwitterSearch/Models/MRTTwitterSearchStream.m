//
//  MRTTwitterStreamingSearchAPI.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <Social/Social.h>

#import "MRTTwitterSearchStream.h"
#import "NSTimer+MRTBlockSupport.h"

static const NSTimeInterval MRTDefaultUpdateInterval = 3.0;
static const NSTimeInterval MRTMinimalUpdateInterval = 1.0;
static const NSUInteger MRTDefaultUpdateLimit = 20;

@interface MRTTwitterSearchStream() <NSURLConnectionDelegate>
@property (nonatomic, strong) NSURLConnection *streamConnection;
@property (nonatomic, strong) NSArray *inputBuffer;
@property (nonatomic, strong) NSTimer *updateTimer;
@property (nonatomic, strong) NSLock *lock;
@property (nonatomic, copy) MRTTwitterSearchStreamUpdateHandler updateHandler;
@end

@implementation MRTTwitterSearchStream

+ (instancetype)streamForSearchKeyword:(NSString *)keyword
                           withAccount:(ACAccount *)account
                      andUpdateHandler:(MRTTwitterSearchStreamUpdateHandler)handler
{
    return [[self alloc] initStreamForSearchKeyword:keyword withAccount:account andUpdateHandler:handler];
}

- (instancetype)initStreamForSearchKeyword:(NSString *)keyword
                               withAccount:(ACAccount *)account
                          andUpdateHandler:(MRTTwitterSearchStreamUpdateHandler)handler
{
    if ([keyword length] == 0 || !account || !handler) return nil;
    if ((self = [super init])) {
        _inputBuffer = [NSArray array];
        _lock = [NSLock new];
        _updateHandler = [handler copy];
        _updateInterval = MRTDefaultUpdateInterval;
        _updateTweetsLimit = MRTDefaultUpdateLimit;
        
        NSDictionary *parameters = @{@"track" : keyword};
        NSURL *requestURL = [NSURL URLWithString:@"https://stream.twitter.com/1.1/statuses/filter.json"];
        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                requestMethod:SLRequestMethodPOST
                                                          URL:requestURL
                                                   parameters:parameters];
        request.account = account;
        
        NSMutableURLRequest *URLRequest = [[request preparedURLRequest] mutableCopy];
        URLRequest.timeoutInterval = 60.0f;
        self.streamConnection = [[NSURLConnection alloc] initWithRequest:[URLRequest copy] delegate:self startImmediately:NO];
    }
    
    return self;
}

- (void)dealloc
{
    [self.updateTimer invalidate];
}

#pragma mark - Custom setters

- (void)setUpdateInterval:(NSTimeInterval)updateInterval
{
    if (_updateInterval != updateInterval && updateInterval > MRTMinimalUpdateInterval) {
        _updateInterval = updateInterval;
        [self scheduleUpdateTimer];
    }
}

- (void)setUpdateTweetsLimit:(NSUInteger)updateTweetsLimit
{
    if ([self.inputBuffer count] > updateTweetsLimit) {
        [self.lock lock];
        self.inputBuffer = [self.inputBuffer subarrayWithRange:NSMakeRange(0, updateTweetsLimit)];
        [self.lock unlock];
    }
    _updateTweetsLimit = updateTweetsLimit;
}


#pragma mark - Public interface

- (void)start
{
    [self.streamConnection start];
    [self scheduleUpdateTimer];
}

- (void)close
{
    [self.updateTimer invalidate];
    [self.streamConnection cancel];
    self.inputBuffer = [NSArray array];
}

#pragma mark - Private methods

- (void)scheduleUpdateTimer
{
    [self.updateTimer invalidate];
    __weak MRTTwitterSearchStream *wSelf = self;
    self.updateTimer = [NSTimer sheduleTimerWithInterval:self.updateInterval
                                                   block:^{
                                                       MRTTwitterSearchStream *stream = wSelf;
                                                       if (stream) {
                                                           [stream invokeUpdateHandler];
                                                       }
                                                   }
                                                 repeats:YES];
}

- (void)invokeUpdateHandler
{
    if (self.updateHandler) {
        [self.lock lock];
        self.updateHandler(self.inputBuffer, nil);
        self.inputBuffer = [NSArray array];
        [self.lock unlock];
    }
}

- (void)appendInputBufferWithArray:(NSArray *)array
{
    if ([array count] > 0) {
        [self.lock lock];
        if ( ([array count] + [self.inputBuffer count]) > self.updateTweetsLimit) {
            array = [array subarrayWithRange:NSMakeRange(0, self.updateTweetsLimit - [self.inputBuffer count])];
        }
        NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:array];
        [tmpArray addObjectsFromArray:self.inputBuffer];
        self.inputBuffer = [tmpArray copy];
        [self.lock unlock];
    }
}

#pragma mark NSURLConnectionDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//	NSLog(@"Connection didReceiveData of length: %ld", (long)data.length);
//    NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    if ([self.inputBuffer count] >= self.updateTweetsLimit) return;
    NSMutableArray *tweetsInfo = [NSMutableArray array];
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    for (NSString *part in [response componentsSeparatedByString:@"\r\n"]) {
        @autoreleasepool {
            if ([part length] == 0) continue;
            NSData *tweetData = [part dataUsingEncoding:NSUTF8StringEncoding];
            id tweetInfo = [NSJSONSerialization JSONObjectWithData:tweetData options:0 error:nil];
            if (tweetInfo && [tweetInfo isKindOfClass:[NSDictionary class]]) {
                [tweetsInfo addObject:tweetInfo];
            }
        }
    }
    [self appendInputBufferWithArray:[tweetsInfo copy]];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"Connection failed! Error - %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    self.updateHandler(nil, error);
}

@end
