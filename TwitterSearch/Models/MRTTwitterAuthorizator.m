//
//  MRTTwitterAuthorizator.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "MRTTwitterAuthorizator.h"

#import <Social/Social.h>

@interface MRTTwitterAuthorizator()
@property (nonatomic, assign) MRTTwitterAuthState state;
@property (nonatomic, strong) ACAccount *twitterAccount;
@property (nonatomic, strong) ACAccountStore *accountStore; //iOS 6 has bug with ACAccountType retaining. We have to keep strong reference
@end

@implementation MRTTwitterAuthorizator

#pragma mark - Singleton Init

+ (MRTTwitterAuthorizator *)sharedInstanse
{
    static dispatch_once_t onceToken;
    static MRTTwitterAuthorizator *sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initUniqueInstance];
        [sharedInstance authorize];
    });
    return sharedInstance;
}

- (MRTTwitterAuthorizator *)initUniqueInstance
{
    if ((self = [super init])) {
        _state = MRTTwitterAuthStateUndetermined;
        _accountStore = [[ACAccountStore alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorize) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    return self;
}

- (void)authorize
{
    self.state = MRTTwitterAuthStateUndetermined;
    self.twitterAccount = nil;
    
    ACAccountType *type = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [self.accountStore requestAccessToAccountsWithType:type
                                               options:NULL
                                            completion:^(BOOL granted, NSError *error) {
                                                if (!granted) {
                                                    BOOL hasAccount = [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
                                                    self.state = (hasAccount) ? MRTTwitterAuthStateErrorAccessDenied : MRTTwitterAuthStateErrorAccountMissing;
                                                }
                                                else {
                                                    NSArray *arrOfAccounts = [self.accountStore accountsWithAccountType:type];
                                                    if ([arrOfAccounts count] > 0) {
                                                        self.twitterAccount = [arrOfAccounts lastObject];
                                                        self.state = MRTTwitterAuthStateSuccess;
                                                    }
                                                    else {
                                                        self.state = MRTTwitterAuthStateErrorAccountMissing;
                                                    }
                                                }
                                            }];
}

@end
