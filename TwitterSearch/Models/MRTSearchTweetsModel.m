//
//  MRTSeachTweetsModel.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "MRTSearchTweetsModel.h"

#import "MRTTwitterSearchStream.h"

@interface MRTSearchTweetsModel()
@property (nonatomic, strong) MRTTwitterSearchStream *stream;
@property (nonatomic, strong) NSArray *tweets;
@property (nonatomic, strong) dispatch_queue_t modelMergeQueue;
@property (nonatomic, strong) NSLock *lock;
@end

@implementation MRTSearchTweetsModel

- (instancetype)init
{
    return [self initWithTwitterAccount:nil];
}

- (instancetype)initWithTwitterAccount:(ACAccount *)account
{
    if ((self = [super init])) {
        _account = account;
        _tweets = [NSArray array];
        _lock = [NSLock new];
        _modelMergeQueue = dispatch_queue_create("com.App.AppTask", DISPATCH_QUEUE_SERIAL);
        dispatch_queue_t high = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
        dispatch_set_target_queue(_modelMergeQueue, high);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stop) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    return self;
}

- (void)searchKeyword:(NSString *)keyword
{
    [self searchKeyword:keyword withTweetSpeedLimitPerSecond:MRTDefaultTweetSpeedLimitPerSercond];
}

- (void)searchKeyword:(NSString *)keyword withTweetSpeedLimitPerSecond:(NSUInteger)limit
{
    NSError *error;
    if (!self.account) {
        error = [[NSError alloc] initWithDomain:@"MRTErrorDomain"
                                           code:200
                                       userInfo:@{NSLocalizedDescriptionKey : @"Fail: Поиск невозможен без аккаунта в Twitter"}];
    }
    
    keyword = [self validateKeyword:keyword];
    if ([keyword length]== 0) {
        error = [[NSError alloc] initWithDomain:@"MRTErrorDomain"
                                           code:201
                                       userInfo:@{NSLocalizedDescriptionKey : @"Fail: Поисковый запрос введен некорректно. Введите хэштег(и) в следующем формате: #хэштег"}];
    }
    
    if (error) {
        [self.delegate searchTweetsModel:self didFailWithError:error];
        return;
    }
    
    self.tweets = [NSArray array];
    [self.delegate searchTweetsModelDidChange:self];
    
    __weak MRTSearchTweetsModel *wSelf = self;
    MRTTwitterSearchStreamUpdateHandler handler = ^(NSArray *newTweetsInfo, NSError *error) {
        if (error) {
            [wSelf.delegate searchTweetsModel:wSelf didFailWithError:error];
        }
        else {
            [wSelf mergeNewTweetsInfo:newTweetsInfo];
        }
    };
    [self.stream close];
    self.stream = [MRTTwitterSearchStream streamForSearchKeyword:keyword
                                                     withAccount:self.account
                                                andUpdateHandler:handler];
    self.stream.updateTweetsLimit = (NSUInteger)(self.stream.updateInterval * limit);
    [self.stream start];
}

- (void)clearTweets
{
    [self.lock lock];
    self.tweets = [NSArray array];
    [self.lock unlock];
    [self.delegate searchTweetsModelDidChange:self];
}

- (void)stop
{
    NSLog(@"Stop");
    [self.stream close];
}

#pragma mark - Private methods

- (NSString *)validateKeyword:(NSString *)keyword
{
    if ([keyword length] == 0) return nil;
    
    NSString *resultPattern = @"";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"((?:#){1}[\\w\\d]{1,140})" options:0 error:&error];
    NSArray *matches = [regex matchesInString:keyword options:0 range:NSMakeRange(0, keyword.length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString *word = [keyword substringWithRange:wordRange];
        if ([resultPattern length] > 0) {
            resultPattern = [resultPattern stringByAppendingString:@","];
        }
        resultPattern = [resultPattern stringByAppendingString:word];
    }
    
    return ([resultPattern length] == 0) ? nil : resultPattern;
}

- (void)mergeNewTweetsInfo:(NSArray *)array
{
    NSInteger tweetCount = [array count];
    if (tweetCount > 0) {
        __weak MRTSearchTweetsModel *wSelf = self;
        dispatch_async(_modelMergeQueue, ^{
            MRTSearchTweetsModel *sSelf = wSelf;
            if (!sSelf) return ;
            [sSelf.lock lock];
            NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
            NSMutableArray *tmpTweets = [NSMutableArray arrayWithCapacity:tweetCount];
            NSUInteger index = 0;
            for (NSInteger i = 0; i < tweetCount; ++i) {
                MRTTweet *tweet = [MRTTweet tweetByTweetInfo:[array objectAtIndex:i]];
                if (tweet) {
                    tweet.tag = [sSelf.tweets count] + i + 1;
                    [tmpTweets addObject:tweet];
                    [indexes addIndex:index];
                    index++;
                }
            }
            [tmpTweets addObjectsFromArray:sSelf.tweets];
            sSelf.tweets = [tmpTweets copy];
            [sSelf.lock unlock];
            dispatch_async(dispatch_get_main_queue(), ^{
                [sSelf.delegate searchTweetsModel:sSelf didRecieveNewTweetsAtIndexes:[indexes copy]];
            });
        });
    }
}

@end
