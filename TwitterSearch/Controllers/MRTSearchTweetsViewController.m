//
//  MRTSearchTweetsViewController.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 31/01/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import "MRTSearchTweetsViewController.h"

#import "MRTTwitterAuthorizator.h"
#import "MRTSearchTweetsModel.h"
#import "MRTTweetTableViewCell.h"
#import "MRTAvatarDownloader.h"

NSString *const kDefaultSearchRequest = @"#apple";

@interface MRTSearchTweetsViewController () <MRTSearchTweetsModelDelegate, NSURLConnectionDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) MRTSearchTweetsModel *model;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *clearButton;
@property (nonatomic, strong) MRTTweetTableViewCell *prototypeCell;
@property (nonatomic, strong) MRTTweetTableViewCell *prototypeLandscapeCell;
@property (nonatomic, strong) NSCache *imageCache;
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, strong) UIImageView *placeholderView;
@end

@implementation MRTSearchTweetsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat searchBarHeight = 44.0f;
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 230, searchBarHeight)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"Input hashtag";
    self.searchBar.text = kDefaultSearchRequest;
    self.searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
    [self.searchBar setSearchFieldBackgroundImage: [[UIImage imageNamed: @"searchbackground"] resizableImageWithCapInsets: UIEdgeInsetsMake(0, 7.0, 0, 7.0)] forState: UIControlStateNormal];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.searchBar setBarTintColor:[UIColor whiteColor]];
    }
    else {
        [self.searchBar setTintColor:[UIColor whiteColor]];
        self.searchBar.layer.borderWidth = 1;
        self.searchBar.layer.borderColor = [[UIColor whiteColor] CGColor];
    }
    
    self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.clearButton.frame = CGRectMake(self.searchBar.frame.size.width, 0, 90, searchBarHeight);
    self.clearButton.enabled = NO;
    self.clearButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.clearButton setTitle:@"Clear" forState:UIControlStateNormal];
    [self.clearButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.clearButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.clearButton addTarget:self action:@selector(clearResults) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect tableFrame = CGRectOffset(self.view.bounds, 0, searchBarHeight);
    tableFrame.size.height -= searchBarHeight;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        tableFrame.size.height -= [[UIApplication sharedApplication] statusBarFrame].size.height;
    }
    self.tableView = [[UITableView alloc] initWithFrame:tableFrame];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MRTTweetTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:@"tweetCell"];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.placeholderView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
    self.placeholderView.frame = CGRectOffset(self.placeholderView.frame, 0, searchBarHeight);
    self.placeholderView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.clearButton];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.placeholderView];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.imageCache = [[NSCache alloc] init];
    self.imageCache.countLimit = 200;
    
    self.downloadQueue = [[NSOperationQueue alloc] init];
    self.downloadQueue.maxConcurrentOperationCount = 3;
    
    self.model = [[MRTSearchTweetsModel alloc] initWithTwitterAccount:[MRTTwitterAuthorizator sharedInstanse].twitterAccount];
    self.model.delegate = self;
    [[MRTTwitterAuthorizator sharedInstanse] addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAppear:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//Fix view offset
- (void)viewDidLayoutSubviews
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        self.edgesForExtendedLayout = UIRectEdgeNone;
        CGRect viewBounds = self.view.bounds;
        CGFloat topBarOffset = self.topLayoutGuide.length;
        viewBounds.origin.y = topBarOffset * -1;
        self.view.bounds = viewBounds;
        self.navigationController.navigationBar.translucent = NO;
    }
}

#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

- (void)clearResults
{
    self.placeholderView.hidden = NO;
    [self.model clearTweets];
    self.clearButton.enabled = NO;
    [self.clearButton setTitle:@"Clear" forState:UIControlStateNormal];
}

#pragma mark - Lazy initialization

- (MRTTweetTableViewCell *)prototypeCell
{
    if (!_prototypeCell) {
        _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"tweetCell"];
    }
    return _prototypeCell;
}

- (MRTTweetTableViewCell *)prototypeLandscapeCell
{
    if (!_prototypeLandscapeCell) {
        _prototypeLandscapeCell = [self.tableView dequeueReusableCellWithIdentifier:@"tweetCell"];
        _prototypeLandscapeCell.frame = CGRectMake(0, 0, self.tableView.frame.size.width, _prototypeLandscapeCell.frame.size.height);
        [_prototypeLandscapeCell layoutSubviews];
    }
    return _prototypeLandscapeCell;
}

#pragma mark - Private methods

- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    if ([self.searchBar.text length] == 0) {
        [self.model stop];
    }
}

- (void)launchSearch
{
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(launchSearch) withObject:nil waitUntilDone:NO];
        return;
    }
    self.placeholderView.hidden = NO;
    if ([self.searchBar.text length] != 0) {
        [self clearResults];
        [self.model searchKeyword:self.searchBar.text];
    }
    [self.view endEditing:YES];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void*)context
{
    if (object == [MRTTwitterAuthorizator sharedInstanse] && [keyPath isEqual:@"state"]) {
        switch ([MRTTwitterAuthorizator sharedInstanse].state) {
            case MRTTwitterAuthStateErrorAccessDenied:
                if (!self.model.account) {
                    [self alertError:[NSError errorWithDomain:@"MRTErrorDomain" code:100 userInfo:@{NSLocalizedDescriptionKey : @"Разрешите доступ к вашему твиттер аккаунту в настройках телефона."}]];
                }
                break;
            case MRTTwitterAuthStateErrorAccountMissing:
                if (!self.model.account) {
                    [self alertError:[NSError errorWithDomain:@"MRTErrorDomain" code:100 userInfo:@{NSLocalizedDescriptionKey : @"Для использования приложения Вам необходимо настроить твиттер аккаунт в настройках телефона."}]];
                }
                break;
            case MRTTwitterAuthStateSuccess:
                self.model.account = [MRTTwitterAuthorizator sharedInstanse].twitterAccount;
                if ([self.searchBar.text isEqualToString:kDefaultSearchRequest]) [self launchSearch];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Keyboard appeare/disappeare handling

- (void)keyboardWillAppear:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect convertedFrame = [self.view convertRect:keyboardFrame fromView:self.view.window];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0f, 0.0, convertedFrame.size.height, 0.0);
    [self.tableView setContentInset:contentInsets];
    [self.tableView setScrollIndicatorInsets:contentInsets];
}

- (void)keyboardWillDisappear:(NSNotification *)notification
{
    [self.tableView setContentInset:UIEdgeInsetsZero];
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsZero];
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    MRTTweetTableViewCell *tmpCell = (UIInterfaceOrientationIsPortrait(orientation)) ? self.prototypeCell : self.prototypeLandscapeCell;
    MRTTweet *tweet = [self.model.tweets objectAtIndex:indexPath.row];
    return [tmpCell cellHeightWithTweetText:tweet.text];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {return 0.01f;}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {return 0.01f;}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.model.tweets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tweetCell";
    MRTTweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    MRTTweet *tweet = [self.model.tweets objectAtIndex:indexPath.row];
    [cell.authorLabel setText:tweet.author];
    [cell.tweetLabel setText:tweet.text];
    [cell.publicationDateLabel setText:[tweet pubDateString]];
    [cell.avatarImageView setImage:nil];
    [cell setTag:tweet.tag];
    
    __weak MRTTweetTableViewCell *wCell = cell;
    MRTTweetAvatarDownloadHandler handler = ^(UIImage *avatar){
        if (wCell.tag == tweet.tag) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [wCell.avatarImageView setImage:avatar];
            });
        }
    };
    [[MRTAvatarDownloader sharedInstanse] loadAsynchronousAvatarByURL:tweet.avatarURL WithCompletionHandler:handler];
    
    return cell;
}

#pragma mark - MRTSearchTweetsModelDelegate

- (void)searchTweetsModel:(MRTSearchTweetsModel *)model didRecieveNewTweetsAtIndexes:(NSIndexSet *)indexes
{
    if ([model.tweets count] == 0 || [indexes count] == 0) return;
    
    if (!self.placeholderView.isHidden) {
        [self.placeholderView setHidden:YES];
        self.clearButton.enabled = YES;
    }
    [self.clearButton setTitle:[NSString stringWithFormat:@"Clear(%ld)", (unsigned long)[self.model.tweets count]] forState:UIControlStateNormal];
    
    NSMutableArray *tmpPaths = [NSMutableArray arrayWithCapacity:[indexes count]];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *path = [NSIndexPath indexPathForItem:idx inSection:0];
        [tmpPaths addObject:path];
    }];
    NSArray *paths = [tmpPaths copy];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGPoint tableViewOffset = [self.tableView contentOffset];
        BOOL offseted = (self.tableView.contentOffset.y > 20.0f);
        if (offseted) {
            tableViewOffset = [self.tableView contentOffset];
            [UIView setAnimationsEnabled:NO];
        }
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        if (offseted) {
            CGFloat heightForNewRows = 0;
            for (NSIndexPath *path in paths) {
                heightForNewRows = heightForNewRows + [self tableView:self.tableView heightForRowAtIndexPath:path];
            }
            tableViewOffset.y += heightForNewRows;
            [UIView setAnimationsEnabled:YES];
            
            [self.tableView setContentOffset:tableViewOffset animated:NO];
        }
    });
}

- (void)searchTweetsModelDidChange:(MRTSearchTweetsModel *)model
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)searchTweetsModel:(MRTSearchTweetsModel *)model didFailWithError:(NSError *)error
{
    [self alertError:error];
    self.clearButton.enabled = NO;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self launchSearch];
}

#pragma mark - Error Handling

- (void)alertError:(NSError *)error
{
    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(alertError:) withObject:error waitUntilDone:NO];
        return;
    }
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

@end
