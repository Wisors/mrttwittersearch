//
//  MRTTweetTableViewCell.m
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "MRTTweetTableViewCell.h"

@interface MRTTweetTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *messageBorderView;
@end

@implementation MRTTweetTableViewCell

- (void)awakeFromNib
{
    self.avatarImageView.layer.cornerRadius = 5.0f;
    self.avatarImageView.clipsToBounds = YES;
    UIImage *backImage = [[UIImage imageNamed:@"message"] resizableImageWithCapInsets:UIEdgeInsetsMake(18.0, 9.0, 3.0, 3.0)];
    [self.messageBorderView setImage:backImage];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (CGFloat)cellHeightWithTweetText:(NSString *)text
{
    NSAttributedString *attributedText =[[NSAttributedString alloc] initWithString:text
                                                                        attributes:@{ NSFontAttributeName: self.tweetLabel.font,
                                                                                      NSForegroundColorAttributeName : self.tweetLabel.backgroundColor}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.tweetLabel.frame.size.width, CGFLOAT_MAX}
                                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               context:nil];
    CGFloat bottomOffset = CGRectGetHeight(self.frame) - CGRectGetHeight(self.tweetLabel.frame) - self.tweetLabel.frame.origin.y;
    return ceilf(rect.size.height) + self.tweetLabel.frame.origin.y + bottomOffset;
}

- (UILabel *)textLabel
{
    return _tweetLabel;
}

@end
