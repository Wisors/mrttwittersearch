//
//  MRTTweetTableViewCell.h
//  TwitterSearch
//
//  Created by Alexandr Nikishin on 01/02/14.
//  Copyright (c) 2014 Alexander Nikishin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRTTweetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationDateLabel;

- (CGFloat)cellHeightWithTweetText:(NSString *)text;

@end
